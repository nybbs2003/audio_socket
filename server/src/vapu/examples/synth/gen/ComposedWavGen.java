package vapu.examples.synth.gen;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ComposedWavGen extends AbstractWavGen {

	private static List<Class<? extends AbstractWavGen>> wav_gens = Arrays.asList(SineWave.class, SawWave.class, TriangularWave.class, SquareWave.class,Noise.class);

	private List<AbstractWavGen> gen_insts;

	public ComposedWavGen() {
		init();
	}

	private void init() {
		gen_insts = new LinkedList<>();
		for (Class<? extends AbstractWavGen> clz : wav_gens) {
			try {
				AbstractWavGen wg = clz.newInstance();
				gen_insts.add(wg);
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void setSrate(float srate) {
		super.setSrate(srate);
		for (WavGen wg : gen_insts) {
			wg.setSrate(srate);
		}
	}

	@Override
	public void setFreq(float freq) {
		super.setFreq(freq);
		for (WavGen wg : gen_insts) {
			wg.setFreq(freq);
		}
	}

	public float getVol() {
		return vol;
	}

	public void setComponentVolume(String name, float vol) {
		for (WavGen wg : gen_insts) {
			if (wg.getClass().getSimpleName().equals(name)) {
				wg.setVol(vol);
				return;
			}
		}
	}

	@Override
	public float calc_wav(float phase) {
		float val = 0;
		for (AbstractWavGen wg : gen_insts) {
			if(wg.vol<=0){
				continue;
			}
			float v = wg.calc_wav(phase)*wg.vol;
			val += v;
		}
		return val;
	}

}
