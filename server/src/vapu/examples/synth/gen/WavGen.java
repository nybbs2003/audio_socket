package vapu.examples.synth.gen;

public interface WavGen {

	public static final float TWOPI = (float) (Math.PI * 2);

	void gen(float[] buffer);

	public abstract void setVol(float vol);

	public abstract void setFreq(float freq);

	public abstract void setSrate(float srate);
}
