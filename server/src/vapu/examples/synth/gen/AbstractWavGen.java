package vapu.examples.synth.gen;

public abstract class AbstractWavGen implements WavGen {

	protected float phase;
	protected float srate;
	protected float freq = 440;
	private float oldFreq = freq;
	protected float vol = 1;
	private float oldVol = vol;

	@Override
	public void setSrate(float srate) {
		this.srate = srate;
	}

	@Override
	public void setFreq(float freq) {
		this.freq = freq;
	}

	@Override
	public void setVol(float vol) {
		this.vol = vol;
	}

	@Override
	public void gen(float[] buffer) {
		float vol_fact = (vol - oldVol) / (buffer.length - 1);
		float v;
		float freq_fact = (freq - oldFreq) / (buffer.length - 1);
		float f;
		for (int i = 0; i < buffer.length; i++) {
			v = oldVol + vol_fact * i;
			f = oldFreq + freq_fact * i;
			float phase_diff = (float) (TWOPI * f / srate);
			phase += phase_diff;
			while (phase > TWOPI) {
				phase -= TWOPI;
			}
			while (phase < 0) {
				phase += TWOPI;
			}
			buffer[i] = calc_wav(phase) * v;
		}
		oldFreq = freq;
		oldVol=vol;
	}
	
	public abstract float calc_wav(float phase);

}
