package vapu.examples.synth.gen;

public class SquareWave extends AbstractWavGen {

	@Override
	public float calc_wav(float phase) {
		if (phase < Math.PI) {
			return 1;
		} else {
			return -1;
		}
	}

}
