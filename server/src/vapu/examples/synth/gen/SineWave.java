package vapu.examples.synth.gen;

public class SineWave extends AbstractWavGen {

	@Override
	public float calc_wav(float phase) {
		return (float) Math.sin(phase);
	}

}
