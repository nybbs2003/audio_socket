package vapu.examples.synth.gen;

import java.util.Random;

public class Noise extends AbstractWavGen {

	private static final Random r = new Random();
	
	@Override
	public float calc_wav(float phase) {
		return (float) r.nextGaussian();
	}

}
