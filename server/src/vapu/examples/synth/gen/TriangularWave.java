package vapu.examples.synth.gen;

public class TriangularWave extends AbstractWavGen{

	@Override
	public float calc_wav(float a) {
		if(a<Math.PI){
			return (float) (-a/Math.PI*2+1);
		}else{
			return (float) ((a-Math.PI)/Math.PI*2-1);
		}
	}
}
