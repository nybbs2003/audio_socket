package vapu.examples.synth.gen;

public class SawWave extends AbstractWavGen {

	@Override
	public float calc_wav(float phase) {
		return phase/TWOPI*2-1;
	}

}
