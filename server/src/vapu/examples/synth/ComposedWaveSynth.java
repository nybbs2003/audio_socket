package vapu.examples.synth;

import im.composer.midi.MidiFreq;
import im.composer.vapu.VAPU;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.json.JSONException;
import org.json.JSONObject;

import themidibus.listener.SimpleMidiListener;
import vapu.examples.synth.gen.ComposedWavGen;

public class ComposedWaveSynth extends VAPU implements SimpleMidiListener {

	private ComposedWavGen[] gens = new ComposedWavGen[128];
	private float vol = 1;

	@Override
	public synchronized void configure(AudioConfiguration context) throws Exception {
		super.configure(context);
		for (int i = 0; i < 128; i++) {
			if (gens[i] == null) {
				gens[i] = new ComposedWavGen();
			}
			gens[i].setSrate(context.getSampleRate());
			gens[i].setFreq((float) MidiFreq.key2freq(i, 0));
			gens[i].setVol(0);
		}
	}

	@Override
	public void activeSensing(long timeStamp) {

	}

	@Override
	public void noteOn(int channel, int pitch, int velocity) {
		gens[pitch].setVol(velocity / 127f);
	}

	@Override
	public void noteOff(int channel, int pitch, int velocity) {
		gens[pitch].setVol(0);
	}

	@Override
	public void controllerChange(int channel, int number, int value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void programChange(int channel, int value) {
		boolean sin_active = ((value & 1) == 1);
		boolean saw_active = ((value & 2) == 2);
		boolean tri_active = ((value & 4) == 4);
		boolean squ_active = ((value & 8) == 8);
		for (ComposedWavGen cvg : gens) {
			cvg.setComponentVolume("SineWave", sin_active ? 1 : 0);
			cvg.setComponentVolume("SawWave", saw_active ? 1 : 0);
			cvg.setComponentVolume("TriangularWave", tri_active ? 1 : 0);
			cvg.setComponentVolume("SquareWave", squ_active ? 1 : 0);
		}
	}

	@Override
	public void pitchBend(int channel, float val) {
		int i = 0;
		for (ComposedWavGen cvg : gens) {
			if (cvg != null) {
				cvg.setFreq((float) MidiFreq.key2freq(i, val));
			}
			i++;
		}
	}

	@Override
	public void channelPressure(int channel, float val) {
		// TODO Auto-generated method stub

	}

	@Override
	public void channelVolume(int channel, float val) {
		vol = val;
	}

	@Override
	public void sustainChange(boolean b) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void executeCommand(long timeStamp, String command) {
		// System.out.println(Arrays.asList(timeStamp,command));
		try {
			JSONObject jobj = new JSONObject(command);
			switch (jobj.optString("cmd")) {
			case "set":
				String component = jobj.optString("key");
				component = component.replace("_vol", "");
				component = wav_name.valueOf(component).toClassSimpleName();
				float volume = (float) jobj.optDouble("val");
				if (!component.isEmpty() && !Float.isNaN(volume)) {
					for (ComposedWavGen cvg : gens) {
						cvg.setComponentVolume(component, volume);
					}
				}
				break;
			}
		} catch (JSONException e) {
		}
	}

	@Override
	protected void computeBuffer() {
		float[] _buf = new float[bufferSize];
		for (ComposedWavGen cvg : gens) {
			if (cvg == null || cvg.getVol() <= 0) {
				continue;
			}
			float[] _buf1 = new float[bufferSize];
			cvg.gen(_buf1);
			for (int i = 0; i < _buf.length; i++) {
				_buf[i] += _buf1[i];
			}
		}
		for (int i = 0; i < bufferSize; i++) {
			_buf[i] *= vol;
			if (_buf[i] > 1) {
				_buf[i] = 1;
			} else if (_buf[i] < -1) {
				_buf[i] = -1;
			}
		}
		for (int i = 0; i < buf.getChannelCount(); i++) {
			float[] __buf = new float[bufferSize];
			System.arraycopy(_buf, 0, __buf, 0, bufferSize);
			buf.setRawChannel(i, __buf);
		}
	}

	private enum wav_name {
		sin {
			@Override
			public String toClassSimpleName() {
				return "SineWave";
			}
		},
		saw {
			@Override
			public String toClassSimpleName() {
				return "SawWave";
			}
		},
		tri {
			@Override
			public String toClassSimpleName() {
				return "TriangularWave";
			}
		},
		squ {
			@Override
			public String toClassSimpleName() {
				return "SquareWave";
			}
		},
		nos {
			@Override
			public String toClassSimpleName() {
				return "Noise";
			}
		};
		public abstract String toClassSimpleName();
	}

}
