package vapu.examples.ddelay;

import javax.sound.midi.MidiUnavailableException;

import im.composer.audio.engine.Source;
import im.composer.vapu.VAPU;
import jass.engine.BufferNotAvailableException;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.json.JSONObject;
import org.tritonus.share.sampled.FloatSampleBuffer;

public class DDelay extends VAPU {

	private float[][] echo;
	private int echoSize = 0;
	private int echoPos = 0;
	private long echoLFODiff = 0;
	private long echoLFODiffMax = 0;
	private float echoLFODepth = 0.8f;
	private float echoFeedback = 0;
	private float echoLFOSpeed = 0;
	private float echoLFOPos = 0;
	private float echoDW = 0.8f;
	private float sampleRate = 44100;

	private float delay_time = 0.01f;
	private float feedback = 0.85f;
	private float lfo_freq = 0.2f;
	private float lfo_depth = 0.5f;
	private float wet_dri_mix = 0.65f;

	@Override
	public synchronized void configure(AudioConfiguration context) throws Exception {
		super.configure(context);
		sampleRate = context.getSampleRate();
		update();
	}

	// Allocate new buffer when modifying buffer size!
	private void setEchoTime(int channels, float millisDelay) {
		int sampleSize = (int) (millisDelay * sampleRate / 1000);
		echoSize = sampleSize;
		if(echo!=null){
			if(channels==echo.length){
				if(echoSize==echo[0].length){
					return;
				}else{
					float[][] _echo = new float[channels][echoSize];
					for(int i=0;i<channels;i++){
						System.arraycopy(echo[i], 0, _echo[i], 0, Math.min(echoSize, echo[i].length));
					}
					echo = _echo;
				}
			}else{
				float[][] _echo = new float[channels][echoSize];
				for(int i=0;i<Math.min(channels, echo.length);i++){
					System.arraycopy(echo[i], 0, _echo[i], 0, Math.min(echoSize, echo[i].length));
				}
				echo = _echo;
			}
		}
		echo = new float[channels][echoSize];
	}

	private void update() {
		int nChannels = Math.min(getContext().getInputChannelCount(), getContext().getOutputChannelCount());
		setEchoTime(nChannels, delay_time * 1000);
		echoFeedback = feedback;
		// Speed of 1 Hz => 2xPI for one revolution!
		// But - depending on number of samples this will be differently large
		// chunks of updates!?
		echoLFOSpeed = (float) (lfo_freq * 2 * Math.PI / sampleRate);
		echoLFODepth = lfo_depth;
		echoLFODiffMax = (long) ((echoSize / 2.0) * echoLFODepth);
		echoLFODiff = 0;
		echoDW = wet_dri_mix;
		echoPos = 0;
	}

	// Generate / Process the sound!
	public void processBuffer(int channel, float[] inBuffer, float[] outBuffer, int sampleFrames) {
		for (int i = 0, n = sampleFrames; i < n; i++) {
			float exVal = inBuffer[i];
			int echoRead = (int) (echoPos + echoLFODiff);

			if (echoRead >= echoSize) {
				echoRead -= echoSize;
			}

			float out = (exVal * (1.0f - echoDW) + echo[channel][echoRead] * echoDW);
			outBuffer[i] = out;

			exVal = exVal + echo[channel][echoRead] * echoFeedback;

			echo[channel][echoPos] = exVal;
			echoPos = (echoPos + 1);
			if (echoPos >= echoSize)
				echoPos = 0;
		}
		// Update LFO - which is a sine!
		echoLFODiff = (int) (echoLFODiffMax * (1.0 + Math.sin(echoLFOPos)));
		echoLFOPos += echoLFOSpeed * sampleFrames;
	}

	@Override
	protected void executeCommand(long timeStamp, String command) {
		JSONObject jobj = new JSONObject(command);
		String key = jobj.optString("key");
		float  val = (float) jobj.optDouble("val");
		switch(key){
		case"delay_time":
			delay_time = val;
			break;
		case "feedback":
			feedback = val;
			break;
		case "lfo_freq":
			lfo_freq = val;
			break;
		case "lfo_depth":
			lfo_depth = val;
			break;
		case "wet_dri_mix":
			wet_dri_mix = val;
			break;
		case "active":
			if(jobj.optBoolean("val")){
				try {
					open();
				} catch (MidiUnavailableException e) {
				}
			}else{
				close();
			}
			break;
		}
		update();
	}

	@Override
	protected void computeBuffer() {
		int nChannels = Math.min(getContext().getInputChannelCount(), getContext().getOutputChannelCount());
		FloatSampleBuffer input = new FloatSampleBuffer(nChannels, getContext().getMaxBufferSize(), getContext().getSampleRate());
		for (Source src : getSources()) {
			try {
				input.mix(src.getBuffer());
			} catch (BufferNotAvailableException e) {
			}
		}
		for (int i = 0; i < nChannels; i++) {
			float[] frames = input.getChannel(i);
			if(isOpen()){
				processBuffer(i, frames, frames, getContext().getMaxBufferSize());
			}
			buf.setRawChannel(i, frames);
		}
	}

}
