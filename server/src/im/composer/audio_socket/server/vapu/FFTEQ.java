package im.composer.audio_socket.server.vapu;

import im.composer.audio.engine.Source;
import im.composer.media.sound.misc.SoundUtil;
import im.composer.vapu.VAPU;
import jass.engine.BufferNotAvailableException;
import jass.generators.FFTFloat;

import java.util.Map;
import java.util.TreeMap;

import javax.sound.midi.MidiUnavailableException;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.tritonus.share.sampled.FloatSampleBuffer;
import org.tritonus.share.sampled.TVolumeUtils;

public class FFTEQ extends VAPU {

	private FFTFloat fft;
	private Map<Integer, Float> freq_vol = new TreeMap<>();
	private float[] weight;

	@Override
	public synchronized void configure(AudioConfiguration context) throws Exception {
		super.configure(context);
		fft = new FFTFloat((int) (Math.log(context.getMaxBufferSize()) / Math.log(2)));
		calc_weight();
	}

	@Override
	protected void executeCommand(long timeStamp, String command) {
		JSONObject jobj = new JSONObject(command);
		String key = jobj.optString("key");
		String val = jobj.optString("val");
		switch (key) {
		case "freq_vol":
			jobj = jobj.optJSONObject("val");
			for (String k : jobj.keySet()) {
				int i = Integer.parseInt(k);
				Float f = (float) jobj.optDouble(k);
				if (!Float.isNaN(f)) {
					freq_vol.put(i, f);
				}
			}
			calc_weight();
			break;
		case "freq_list":
			JSONArray jarr = new JSONArray(val);
			freq_vol.clear();
			for (int i = 0; i < jarr.length(); i++) {
				freq_vol.put(jarr.getInt(i), 0f);
			}
			break;
		case "active":
			if (jobj.optBoolean("val")) {
				try {
					open();
				} catch (MidiUnavailableException e) {
				}
			} else {
				close();
			}
			break;
		}
	}

	private void calc_weight() {
		if (weight == null || weight.length != getContext().getMaxBufferSize()) {
			weight = new float[getContext().getMaxBufferSize()];
		}
		if (freq_vol.isEmpty()) {
			for (int i = 0; i < weight.length; i++) {
				weight[i] = 1;
			}
		} else if (freq_vol.size() == 1) {
			float val = freq_vol.entrySet().iterator().next().getValue();
			val = (float) TVolumeUtils.log2lin(val);
			for (int i = 0; i < weight.length; i++) {
				weight[i] = val;
			}
		} else {
			TreeMap<Integer, Float> freq_vol = new TreeMap<>();
			freq_vol.putAll(this.freq_vol);
			if (freq_vol.firstKey() != 0) {
				freq_vol.put(0, freq_vol.firstEntry().getValue());
			}
			if (freq_vol.lastKey() != getContext().getSampleRate() / 2) {
				freq_vol.put((int) (getContext().getSampleRate() / 2), freq_vol.lastEntry().getValue());
			}
			float[] fft_index_freq = new float[bufferSize / 2];
			for (int i = 0; i < fft_index_freq.length; i++) {
				fft_index_freq[i] = getContext().getSampleRate() / bufferSize * i;
			}
			int[] index = new int[freq_vol.size()];
			int i = 0;
			for (int freq : freq_vol.keySet()) {
				float min_diff = Float.MAX_VALUE;
				int _index = -1;
				for (int j = 0; j < fft_index_freq.length; j++) {
					float diff = Math.abs(fft_index_freq[j] - freq);
					if (diff < min_diff) {
						min_diff = diff;
						_index = j;
					}
					if(min_diff==0){
						break;
					}
				}
				index[i] = _index;
				i++;
			}
			Integer[] freq_arr = new Integer[freq_vol.size()];
			freq_vol.keySet().toArray(freq_arr);
			for (int j = 0; j < index.length - 1; j++) {
				int index1 = index[j];
				int index2 = index[j + 1];
				float value1 = freq_vol.get(freq_arr[j]);
				float value2 = freq_vol.get(freq_arr[j+1]);
				float diff = (value2 - value1) / (index2 - index1);
				for (i = index1; i <= index2; i++) {
					weight[i] = value1 + (i - index1) * diff;
				}
			}
			for (i = 0; i < weight.length; i++) {
				weight[i] = (float) TVolumeUtils.log2lin(weight[i]);
			}
		}
	}

	@Override
	protected void computeBuffer() {
		FloatSampleBuffer input = new FloatSampleBuffer(getContext().getInputChannelCount(), getContext().getMaxBufferSize(), getContext().getSampleRate());
		for (Source src : getSources()) {
			try {
				input.mix(src.getBuffer());
			} catch (BufferNotAvailableException e) {
			}
		}
		if (getContext().getInputChannelCount() != getContext().getOutputChannelCount()) {
			input.mixDownChannels();
			float[] samples = input.getChannel(0);
			for (int i = 0; i < bufferSize; i++) {
				samples[i] = samples[i] / getContext().getInputChannelCount();
			}
			FloatSampleBuffer buf = new FloatSampleBuffer(getContext().getOutputChannelCount(), getContext().getMaxBufferSize(), getContext().getSampleRate());
			for (int i = 0; i < buf.getChannelCount(); i++) {
				buf.setRawChannel(i, samples);
			}
			input = buf;
		}
		for (int i = 0; i < getContext().getOutputChannelCount(); i++) {
			float[] xr = input.getChannel(i);
			if (isOpen()) {
				float[] xi = new float[bufferSize];
				fft.doFFT(xr, xi, true);
				for (int j = 1; j < bufferSize; j++) {
					xr[j] *= weight[j];
					xi[j] *= weight[j];
				}
				fft.doFFT(xr, xi, false);
			}
			SoundUtil.applyVolumeLimit(xr,-1,+1);
			float[] output = buf.getChannel(i);
			System.arraycopy(xr, 0, output, 0, bufferSize);
		}
	}

}
