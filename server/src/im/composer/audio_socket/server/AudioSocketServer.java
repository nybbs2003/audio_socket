package im.composer.audio_socket.server;

import im.composer.vapu.adapter.GervillSynth;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import javax.sound.midi.ShortMessage;
import javax.websocket.server.ServerContainer;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;
import org.jaudiolibs.audioservers.AudioConfiguration;

public class AudioSocketServer {
	public static final ExecutorService es = Executors.newCachedThreadPool();

	public static void main(String[] args){
		es.submit(new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				Server server = new Server();
				ServerConnector connector = new ServerConnector(server);
				connector.setPort(8080);
				server.addConnector(connector);

				ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
				context.setContextPath("/");
				server.setHandler(context);

				ServerContainer wscontainer = WebSocketServerContainerInitializer.configureContext(context);

				wscontainer.addEndpoint(WebSocketAudioServer.class);

				server.start();
				return null;
			}
		});
		es.submit(new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				GervillSynth ger_synth = new GervillSynth();
				ger_synth.configure(new AudioConfiguration(32000, 1, 1, 128, true));
				ger_synth.send(new ShortMessage(ShortMessage.NOTE_ON, 0, 1), -1);
				ger_synth.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, 0), -1);
				ger_synth.getBuffer(1);
				ger_synth.close();
				Logger.getGlobal().info(GervillSynth.class.getSimpleName()+" inited!");
				return null;
			}
		});
	}
}
