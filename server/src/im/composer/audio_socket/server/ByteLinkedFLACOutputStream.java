package im.composer.audio_socket.server;

import java.io.IOException;

import javaFlacEncoder.FLACOutputStream;

public class ByteLinkedFLACOutputStream implements FLACOutputStream {

	private final ByteLinkedOutputStream blos = new ByteLinkedOutputStream();
	@Override
	public void close() throws IOException {
		blos.close();
	}

	@Override
	public long seek(long pos) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public int write(byte[] data, int offset, int count) throws IOException {
		blos.write(data, offset, count);
		return count;
	}

	@Override
	public long size() {
		return blos.size();
	}

	@Override
	public void write(byte data) throws IOException {
		blos.write(data);
	}

	@Override
	public boolean canSeek() {
		return false;
	}

	@Override
	public long getPos() {
		return blos.size();
	}

	public void reset() {
		blos.reset();
	}

	public byte[] toByteArray() {
		return blos.toByteArray();
	}

	@Override
	public String toString() {
		return blos.toString();
	}

}
