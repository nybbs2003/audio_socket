package im.composer.audio_socket.server;

import im.composer.audio_socket.server.misc.DaemonThreadFactory;
import im.composer.audio_socket.server.misc.MyMap;
import im.composer.audio_socket.server.misc.SkippingPipedOut;
import im.composer.audio_socket.server.vapu.FFTEQ;
import im.composer.media.sound.misc.PipedOut;
import im.composer.media.sound.render.SourcePlayer;
import im.composer.midi.remote.RawMidiMessage;
import im.composer.midi.seq.SequencerAdapter;
import im.composer.vapu.VAPU;
import im.composer.vapu.adapter.GervillSynth;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javaFlacEncoder.FLACEncoder;
import javaFlacEncoder.StreamConfiguration;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.sampled.AudioFormat;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.tritonus.share.sampled.AudioUtils;
import org.tritonus.share.sampled.FloatSampleBuffer;

import vapu.examples.ddelay.DDelay;

@ServerEndpoint(value = "/audio_server/")
public class WebSocketAudioServer implements AudioServer, Runnable {
	private ExecutorService es;
	private MyMap vapu_reg;
	private Session session;
	private AudioConfiguration audioContext;
	private boolean active = false;
	private boolean pause = false;
	private SourcePlayer player;
	private FLACEncoder enc;
	private ByteLinkedFLACOutputStream stream;

	@OnOpen
	public void onWebSocketConnect(Session sess) {
		es = Executors.newSingleThreadExecutor(new DaemonThreadFactory());
		session = sess;
		vapu_reg = new MyMap();
		player = new SourcePlayer();
		enc = new FLACEncoder();
	}

	@OnMessage
	public void onWebSocketText(String message) {
		es.submit(new TextWorker(message));
	}

	private class TextWorker implements Callable<Void> {

		private final String message;

		public TextWorker(String message) {
			super();
			this.message = message;
		}

		@Override
		public Void call() throws Exception {
			JSONObject jobj = new JSONObject(message);
			if (jobj.optBoolean("resume")) {
				active = true;
				pause = false;
			} else if (jobj.optBoolean("pause")) {
				pause = true;
			} else if (jobj.optBoolean("reset")) {
				active = false;
				pause = false;
			}
			switch (jobj.optString("cmd")) {
			case "init_audio_input":
				new BinaryWorker(new byte[0]).init_pipe();
				break;
			case "init_synth":
				init_synth(jobj.optString("clz"));
				break;
			case "conf":
				conf(jobj.getJSONObject("data"));
				break;
			case "step":
				step();
				break;
			case "midi_input":
				receive_midi_input(jobj.getJSONArray("data"));
				break;
			case "midi_file":
				buildSynth(jobj.optString("data"));
				break;
			case "cmd":
				command_received(jobj);
				break;
			case "set":
//				System.out.println(Arrays.asList(message,vapu_reg));
				set_property(jobj);
				break;
			}
			return null;
		}

		private void buildSynth(String data) throws Exception {
			VAPU vapu = vapu_reg.getVAPU("midi_file_synth");
			if (vapu == null) {
				if (data.startsWith("data:audio/midi;base64,")) {
					data = data.substring("data:audio/midi;base64,".length());
				} else {
					return;
				}
				byte[] b_arr = Base64.getDecoder().decode(data);
				Sequence seq = MidiSystem.getSequence(new ByteArrayInputStream(b_arr));
				SequencerAdapter seq_adp = new SequencerAdapter();
				seq_adp.setSequence(seq);
				GervillSynth ger_synth = new GervillSynth();
				ger_synth.addSource(seq_adp);
				seq_adp.setReceiver(ger_synth);
				player.addSource(ger_synth);
				ger_synth.configure(audioContext);
				ger_synth.setTime(player.getTime());
				seq_adp.open();
				seq_adp.start();
				vapu_reg.put("midi_file_synth", ger_synth);
			}
		}

		private void receive_midi_input(JSONArray arr) {
			VAPU vapu = vapu_reg.getVAPU("user_synth");
			if (vapu != null) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream(arr.length());
				for (int i = 0; i < arr.length(); i++) {
					int a = arr.getInt(i);
					baos.write(a);
				}
				byte[] b_arr = baos.toByteArray();
				RawMidiMessage msg = new RawMidiMessage(b_arr);
				vapu.send(msg, -1);
			}
		}
		private void command_received(JSONObject jobj){
			String id = jobj.optString("id");
			if (id.isEmpty()) {
				return;
			}
			VAPU vapu = vapu_reg.getVAPU(id);
			if (vapu != null) {
				vapu.commandReceived(-1, jobj.optString("data"));
			}
		}
		
		private void set_property(JSONObject jobj){
			String id = jobj.optString("id");
			if (id.isEmpty()) {
				return;
			}
			VAPU vapu = vapu_reg.getVAPU(id);
			if (vapu != null) {
				String key = jobj.optString("key");
				String val = jobj.optString("val");
				if (!Arrays.asList(key, val).contains("")) {
					vapu.commandReceived(-1, message);
				}
			}
		}
	}

	@OnMessage
	public void onWebSocketBinary(byte[] b_arr) {
		es.submit(new BinaryWorker(b_arr));
	}

	private class BinaryWorker implements Callable<Void> {

		private final byte[] b_arr;

		public BinaryWorker(byte[] b_arr) {
			super();
			this.b_arr = b_arr;
		}

		@Override
		public Void call() throws Exception {
			PipedOut piped_out = (PipedOut) vapu_reg.get("piped_out");
			if (piped_out == null) {
				piped_out = init_pipe();
			}
			FloatSampleBuffer fsb = new FloatSampleBuffer(b_arr, 0, b_arr.length, new AudioFormat(audioContext.getSampleRate(), 16, audioContext.getInputChannelCount(), true, false));
			piped_out.put(fsb);
			return null;
		}

		public PipedOut init_pipe() throws Exception {
			PipedOut piped_out = new SkippingPipedOut();
			piped_out.configure(audioContext);
			piped_out.setTime(player.getTime());
			FFTEQ ffteq = new FFTEQ();
			ffteq.configure(audioContext);
			ffteq.setTime(player.getTime());
			ffteq.addSource(piped_out);
			DDelay delay = new DDelay();
			delay.configure(audioContext);
			delay.setTime(player.getTime());
			delay.addSource(ffteq);
			player.addSource(delay);
			vapu_reg.put("delay_1", delay);
			vapu_reg.put("eq_1", ffteq);
			vapu_reg.put("piped_out", piped_out);
			return piped_out;
		}
	}

	private void init_synth(String clz) throws Exception {
		Class<?> clazz = Class.forName(clz);
		if (!isSubclassOf(clazz, VAPU.class)) {
			throw new IllegalArgumentException(clz + " is NOT VAPU!");
		}
		VAPU user_synth = (VAPU) clazz.newInstance();
		FFTEQ ffteq = new FFTEQ();
		ffteq.configure(audioContext);
		ffteq.setTime(player.getTime());
		ffteq.addSource(user_synth);
		DDelay delay = new DDelay();
		delay.configure(audioContext);
		delay.setTime(player.getTime());
		delay.addSource(ffteq);
		vapu_reg.put("delay_0", delay);
		player.addSource(delay);
		vapu_reg.put("eq_0", ffteq);
		vapu_reg.put("user_synth", user_synth);
	}

	private static boolean isSubclassOf(Class<?> a, Class<?> b) {
		if (Object.class.equals(b)) {
			return true;
		}
		while (true) {
			Class<?> c = a.getSuperclass();
			if (Object.class.equals(c)) {
				return false;
			}
			if (c.equals(b)) {
				return true;
			}
			a = c;
		}
	}

	private void conf(JSONObject jobj) {
		int bufz = jobj.getInt("bufz");
		int srate = jobj.getInt("srate");
		int ins = jobj.getInt("ins");
		int outs = jobj.getInt("outs");
		audioContext = new AudioConfiguration(srate, ins, outs, bufz, true);
		try {
			player.configure(audioContext);
			enc.setStreamConfiguration(new StreamConfiguration(audioContext.getOutputChannelCount(), audioContext.getMaxBufferSize(), audioContext.getMaxBufferSize(), (int) audioContext
					.getSampleRate(), 16));
			stream = new ByteLinkedFLACOutputStream();
			enc.setOutputStream(stream);
			enc.openFLACStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private synchronized void step() {
		if (!active || pause) {
			return;
		}
		List<FloatBuffer> inputs = prepareBuffer(audioContext.getInputChannelCount(), audioContext.getMaxBufferSize());
		List<FloatBuffer> outputs = prepareBuffer(audioContext.getOutputChannelCount(), audioContext.getMaxBufferSize());
		player.process(-1, inputs, outputs, audioContext.getMaxBufferSize());
		byte[] b_arr = encode(outputs);
		ByteBuffer buf = ByteBuffer.wrap(b_arr);
		try {
			session.getAsyncRemote().sendBinary(buf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private byte[] encode(List<FloatBuffer> outputs) {
		byte[] b_arr = null;
		int[] samples = new int[audioContext.getMaxBufferSize() * audioContext.getOutputChannelCount()];
		for (int i = 0; i < outputs.size(); i++) {
			float[] f_arr = outputs.get(i).array();
			for (int j = 0; j < audioContext.getMaxBufferSize(); j++) {
				int k = j * outputs.size() + i;
				samples[k] = (int) (f_arr[j] * Short.MAX_VALUE);
			}
		}
		try {
			enc.addSamples(samples, audioContext.getMaxBufferSize());
			enc.encodeSamples(audioContext.getMaxBufferSize(), false);
			b_arr = stream.toByteArray();
			stream.reset();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return b_arr;
	}

	private List<FloatBuffer> prepareBuffer(int channelCount, int bufferSize) {
		List<FloatBuffer> list = new ArrayList<FloatBuffer>();
		for (int i = 0; i < channelCount; i++) {
			FloatBuffer buf = FloatBuffer.allocate(bufferSize);
			list.add(buf);
		}
		return list;
	}

	@OnError
	public void onWebSocketError(Throwable cause) {
		cause.printStackTrace();
	}

	@OnClose
	public void onWebSocketClose(CloseReason reason) {
		// System.out.println("close:"+reason);
		shutdown();
	}

	@Override
	public void run() {
		long millisecond_per_frame = (long) AudioUtils.frames2MillisD(audioContext.getMaxBufferSize(), audioContext.getSampleRate());
		while (active) {
			step();
			try {
				Thread.sleep(millisecond_per_frame);
			} catch (InterruptedException e) {
				shutdown();
			}
		}
	}

	@Override
	public AudioConfiguration getAudioContext() {
		return audioContext;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void shutdown() {
		active = false;
		vapu_reg.clear();
		es.shutdown();
	}

}
