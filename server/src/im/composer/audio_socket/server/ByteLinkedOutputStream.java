package im.composer.audio_socket.server;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;

public class ByteLinkedOutputStream extends OutputStream {

	private final LinkedList<byte[]> list = new LinkedList<byte[]>();
	private int arr_size, pos = 0;
	private byte[] arr;
	private boolean closed = false;

	public ByteLinkedOutputStream(int arr_size) {
		super();
		this.arr_size = arr_size;
		init();
	}

	public ByteLinkedOutputStream() {
		this(32);
	}

	private void init() {
		arr = new byte[arr_size];
		pos = 0;
	}

	private void push() {
		list.add(arr);
		init();
	}

	public void reset() {
		if(closed){
			throw new IllegalStateException("Stream closed!");
		}
		list.clear();
		init();
	}

	@Override
	public void close() throws IOException {
		closed = true;
	}

	@Override
	public void write(int b) throws IOException {
		if(closed){
			throw new IOException("Stream closed!");
		}
		arr[pos]=(byte) (b&0xFF);
		pos++;
		if(pos>arr_size-1){
			push();
		}
	}
	
	public int size(){
		int arr_size = 0;
		for(byte[] b:list){
			arr_size+=b.length;
		}
		arr_size+=pos;
		return arr_size;
	}

	public byte[] toByteArray(){
		byte[] arr = new byte[size()];
		int pos = 0;
		for(byte[] b:list){
			System.arraycopy(b, 0, arr, pos, b.length);
			pos+=b.length;
		}
		System.arraycopy(this.arr, 0, arr, pos, this.pos);
		return arr;
	}

	@Override
	public String toString() {
		return new String(toByteArray());
	}
}
