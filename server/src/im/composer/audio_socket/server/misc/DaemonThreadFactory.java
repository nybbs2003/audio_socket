package im.composer.audio_socket.server.misc;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class DaemonThreadFactory implements ThreadFactory {
	private static final AtomicInteger i = new AtomicInteger();
	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r,"DaemonThread-"+i.getAndIncrement());
		t.setDaemon(true);
		return t;
	}

}
