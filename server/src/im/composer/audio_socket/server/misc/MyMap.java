package im.composer.audio_socket.server.misc;

import im.composer.audio.engine.Out;
import im.composer.audio.engine.Sink;
import im.composer.vapu.VAPU;

import java.util.TreeMap;

@SuppressWarnings("serial")
public class MyMap extends TreeMap<String, Object> {

	public Sink getSink(String key){
		Object o = get(key);
		if(o instanceof Sink){
			return (Sink) o;
		}
		return null;
	}

	public Out getOut(String key){
		Object o = get(key);
		if(o instanceof Out){
			return (Out) o;
		}
		return null;
	}
	
	public VAPU getVAPU(String key){
		Object o = get(key);
		if(o instanceof VAPU){
			return (VAPU) o;
		}
		return null;
	}
}
