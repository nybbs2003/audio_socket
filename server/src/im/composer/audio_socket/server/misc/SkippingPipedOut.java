package im.composer.audio_socket.server.misc;

import im.composer.media.sound.misc.PipedOut;

import java.util.Deque;
import java.util.LinkedList;

import org.apache.http.annotation.NotThreadSafe;
import org.tritonus.share.sampled.FloatSampleBuffer;
import org.tritonus.share.sampled.TVolumeUtils;

@NotThreadSafe
public class SkippingPipedOut extends PipedOut {
	private int min_queue_size=2,max_queue_size = 5;
	private double min_vol_log = -180;
	@Override
	protected void computeBuffer() {
		skipIfNecessary();
		super.computeBuffer();
	}

	private void skipIfNecessary() {
		if(q.size()<max_queue_size){
			return;
		}
		FloatSampleBuffer head = q.pop();
		Deque<FloatSampleBuffer> tail = new LinkedList<>();
		for(int i=0;i<min_queue_size;i++){
			tail.offerFirst(q.pollLast());
		}
		q.clear();
		FloatSampleBuffer tail1 = crossfade(head,tail.pop());
		tail.offerFirst(tail1);
		while(!tail.isEmpty()){
			q.offerFirst(tail.pollLast());
		}
		
	}

	private FloatSampleBuffer crossfade(FloatSampleBuffer head, FloatSampleBuffer tail) {
		FloatSampleBuffer fsb = new FloatSampleBuffer(head.getChannelCount(),head.getSampleCount(),head.getSampleRate());
		for(int i=0;i<fsb.getChannelCount();i++){
			float[] sample = crossfade(head.getChannel(i),tail.getChannel(i));
			fsb.setRawChannel(i, sample);
		}
		return fsb;
	}

	private float[] crossfade(float[] head, float[] tail) {
		int len = head.length;
		float[] result = new float[len];
		for(int i=0;i<len;i++){
			float a = head[i];
			float b = tail[i];
			float ratio = (float) TVolumeUtils.lin2log(min_vol_log/len*i);
			float c = a*ratio+b*(1-ratio);
			if(c>1){
				c=1;
			}else if(c<-1){
				c=-1;
			}
			result[i] = c;
		}
		return result;
	}

}
