var max_buf_count = 5;
var max_buf_count_down_limit = 5;
var max_buf_count_up_limit = 10;
function create_remote_node(context,bufz,in_channel_count,out_channel_count,remote_url,synth_clz){
  var node = context.createScriptProcessor(bufz, in_channel_count, out_channel_count);
  node.on_open = function(){
    var evt = document.createEvent('Event');
    evt.initEvent('open');
    node.dispatchEvent(evt);
  };
  node.on_closed = function(){
    var evt = document.createEvent('Event');
    evt.initEvent('close');
    node.dispatchEvent(evt);
  };
  var asset = AV.Asset.fromWebSocket(remote_url);
  var ws = asset.source.socket;
  var audio_buf = [];
  var last_buf;
  var pause = false;
  asset.on("data",function(e){
    audio_buf.push(e);
  });
  asset.on("error",function(e){
    console.error(e);
    player_stop();
  });
  asset.source.on("open",function(){
    conf_audio();
    init_audio_input();
    init_synth(synth_clz);
    node.on_open();
    set_player_status('running');
  });
  asset.source.on("close",function(){
    node.on_closed();
  });
  function init_audio_input(){
    if(local_audio_input_enabled){
      var cmd = {};
      cmd["cmd"] = "init_audio_input";
      ws.send(JSON.stringify(cmd));
    }
  }
  function init_synth(clz){
    var cmd = {};
      cmd["cmd"] = "init_synth";
      cmd["clz"]=clz;
      ws.send(JSON.stringify(cmd));
  }
  function conf_audio(){
      var cmd = {};
      cmd["cmd"] = "conf";
      var audio_conf = {};
      audio_conf["bufz"]=bufz;
      audio_conf["srate"]=context.sampleRate;
      audio_conf["ins"]=in_channel_count;
      audio_conf["outs"]=out_channel_count;
      cmd["data"]=audio_conf;
      ws.send(JSON.stringify(cmd));
  }
  asset.start();
  function add_buffer(){
    if(audio_buf.length<max_buf_count_down_limit){
      max_buf_count = max_buf_count_up_limit;
    }
    if(audio_buf.length>max_buf_count_up_limit){
      max_buf_count--;
    }
    if(max_buf_count<max_buf_count_down_limit){
      max_buf_count=max_buf_count_down_limit;
    }
    if(audio_buf.length<max_buf_count){
      var cmd = {};
      cmd["cmd"] = "step";
      ws.send(JSON.stringify(cmd));
    }
    setTimeout(function () {
      set_buf_count_bar_val(audio_buf.length,max_buf_count);
    }, 0);
  }
  if(local_audio_input_enabled){
    var getUserMedia = navigator.getUserMedia ? 'getUserMedia' :
    navigator.webkitGetUserMedia ? 'webkitGetUserMedia' :
    navigator.mozGetUserMedia ? 'mozGetUserMedia' :
    navigator.msGetUserMedia ? 'msGetUserMedia' :
    undefined;
    var conditions={audio:true, video:false};
        navigator[getUserMedia](
        conditions,
        function(stream) {
            var micsrc=context.createMediaStreamSource(stream);
            micsrc.connect(node);
        },
        function(e) { console.error(e); }
    );
  }
  node.onaudioprocess = function(e) {
    process_input(e);
    process_output(e);
  };
  function process_input(e){
    if(local_audio_input_enabled&&ws.readyState===1){
      var i,j,k,max_vol=0;
      for(i=0;i<in_channel_count;i++){
        var vol = get_max_vol(e.inputBuffer.getChannelData(i));
        if(vol>max_vol){
          max_vol = vol;
        }
      }
      var mic_gain_factor=Math.pow(10,mic_gain/20);
      max_vol*=mic_gain_factor;
      set_mic_vol_bar(max_vol);
      if(max_vol<min_vol_to_send){
        return;
      }
      var out = new Int16Array(in_channel_count*bufz);
      for(i=0;i<in_channel_count;i++){
        var _inBuf = e.inputBuffer.getChannelData(i);
        for(j=0;j<bufz;j++){
          k = _inBuf[j];
          k *=mic_gain_factor;
          if(k>1){
            k=1;
          }else if(k<-1){
            k=-1;
          }
          k *=0x7FFF;
          out[i+j*in_channel_count]=k;
        }
      }
		  ws.send(out);
    }
  }
  function isSilence(buf){
    for(var i=0;i<buf.length;i++){
      if(buf[i]!==0){
        return false;
      }
    }
    return true;
  }
  
  function get_max_vol(buf){
    if(buf===undefined){
      return 0;
    }
    var max = 0;
    for(var i=0;i<bufz;i++){
      var a = Math.abs(buf[i]);
      if(a>max){
        max=a;
      }
    }
    return max;
  }
  
  function process_output(e){
    var i,j,k,output;
    if(pause||ws.readyState!=1){
      for(i=0;i<out_channel_count;i++){
        output = e.outputBuffer.getChannelData(i);
        for(j=0;j<bufz;j++){
          output[j]=0;
        }
      }
      set_main_vol_bar(0);
      return;
    }
    add_buffer();
    if(audio_buf.length>0){
      last_buf = audio_buf.shift();
    }
    process_gain(last_buf);
    set_main_vol_bar(get_max_vol(last_buf));
    for(i=0;i<out_channel_count;i++){
      output = e.outputBuffer.getChannelData(i);
      for(j=0;j<bufz;j++){
        k = i*bufz+j;
        if(last_buf === undefined){
          output[j] = 0;
        }else{
          output[j] = last_buf[k];
        }
      }
    }
  }
  node.send_midi = function(midi_data){
    if(ws.readyState===1){
      var cmd = {};
      cmd["cmd"]="midi_input";
      cmd["data"]=midi_data;
      ws.send(JSON.stringify(cmd));
    }
  };
  node.send_midi_file = function(midi_file_content){
    if(ws.readyState===1){
      var cmd = {};
      cmd["cmd"]="midi_file";
      cmd["data"]=midi_file_content;
      ws.send(JSON.stringify(cmd));
    }
  };
  node.send_cmd = function(id,cmd_data){
    if(ws.readyState){
      var cmd = {};
      cmd["cmd"]="cmd";
      cmd["id"]=id;
      cmd["data"]=cmd_data;
      ws.send(JSON.stringify(cmd));
    }
  };
  node.set_property = function(id,key,val){
    if(ws.readyState){
      var cmd = {};
      cmd["cmd"]="set";
      cmd["id"]=id;
      cmd["key"]=key;
      cmd["val"]=val;
      ws.send(JSON.stringify(cmd));
    }
  };
  node.start = function(){
    pause = false;
    asset.source.start();
  };
  node.pause = function(){
    pause = true;
    asset.source.pause();
  };
  node.reset = function(){
    pause = true;
    asset.source.reset();
    ws.close();
  };
  return node;
}