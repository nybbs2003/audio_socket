var midi_file_content;
var midi_content_sent = false;
var params = {
	fileInput: $("#doc-form-file").get(0),
	dragDrop: $("#fileDragArea").get(0),
	filter: function(files) {
		var arrFiles = [];
		for (var i = 0, file; i<files.length; i++) {
		  file = files[i];
      if (file.type.indexOf("audio/midi") === 0) {
          if (file.size >= 512000) {
              alert('您这"'+ file.name +'"文件大小过大，应小于500k。');	
          } else {
              arrFiles.push(file);	
          }			
        } else {
          alert('文件"' + file.name + '"不是MIDI文件。');	
        }
		}
		return arrFiles;
	},
	onSelect: function(files) {
		file = files.pop();
		files = [];
		if (file) {
      $('#file-list').html('<span class="am-badge">' + file.name + '</span>');
			var reader = new FileReader();
			reader.onload = function(e) {
			  midi_file_content = e.target.result;
			};
			reader.readAsDataURL(file);
		}
	},
	onDragOver: function() {
		$(this).addClass("upload_drag_hover");
	},
	onDragLeave: function() {
		$(this).removeClass("upload_drag_hover");
	}
};
ZXXFILE = $.extend(ZXXFILE, params);
ZXXFILE.init();
function clear_midi_selection(){
  file = undefined;
  midi_file_content = undefined;
  midi_content_sent = false;
  $('#file-list').html('');
}
function send_midi_content(){
  if(midi_file_content){
    node.send_midi_file(midi_file_content);
    midi_content_sent = true;
  }
}
function reset_midi_content_sent(){
  midi_content_sent = false;
}
function add_send_midi_listsner(){
  node.removeEventListener('open',send_midi_content);
  node.addEventListener('open',send_midi_content);
  node.removeEventListener('close',reset_midi_content_sent);
  node.addEventListener('close',reset_midi_content_sent);
}