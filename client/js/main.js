var audio_server_url = "ws://localhost:8080/audio_server/";
var audioContext = new AudioContext();
var node;
window.addEventListener('midiin-event:foo-input', function(event) {
  if(event.detail.data.length===1){
    var a = event.detail.data[0];
    if(a===0xF8||a===0xFE){
      return;
    }
  }
  if(node){
    var arr = [];
    for(var i=0; i<event.detail.data.length; i++) {
      arr[i] = event.detail.data[i];
    }
    node.send_midi(arr);
  }
});
function player_start(){
  set_player_status('prepare');
  set_mic_vol_bar(0);
  if(node){
    node.start();
    set_player_status('running');
  }else{
    node = create_remote_node(audioContext,document.getElementById('bufz_select').value,1,1,audio_server_url,document.getElementById('synth_clz').value);
    node.connect(audioContext.destination);
    node.addEventListener('open',function(){
      for(var i=0;i<=1;i++){
        init_eq_remote(i);
        init_delay_remote(i);
      }
    });
  }
  audioContext.resume();
  document.getElementById('bufz_select').disabled=1;
  document.getElementById('synth_clz').disabled=1;
}
function player_pause(){
  set_mic_vol_bar(0);
  if(node){
    node.pause();
  }
  audioContext.suspend();
  set_mic_vol_bar(0);
  set_player_status('pause');
}
function player_stop(){
  set_mic_vol_bar(0);
  if(node){
    node.reset();
    node = undefined;
  }
  document.getElementById('synth_clz').disabled=0;
  document.getElementById('bufz_select').disabled=0;
  audioContext.suspend();
  set_mic_vol_bar(0);
  set_player_status('stop');
  setTimeout(function () {
    set_buf_count_bar_val(0,1);
  }, 0);

}
$(document).ready(function(){
  set_player_status('init');
});
$(document).ready(function(){
  $("#synth_clz").change(function(){
    if(this.value==='vapu.examples.synth.ComposedWaveSynth'){
      $("#ComposedWaveSynth_options").css("display","inherit");
    }else{
      $("#ComposedWaveSynth_options").css("display","none");
    }
  });
  $("#min_buffer_count").spinner({
    spen:1,
    min:2,
    max:10,
    change:change_max_buf_count_down_limit,
    stop:change_max_buf_count_down_limit,
  });
  $("#min_buffer_count").spinner("value","5");
  $("#max_buffer_count").spinner({
    step:1,
    min:5,
    max:10,
    change:change_max_buf_count_up_limit,
    stop:change_max_buf_count_up_limit,
  });
  $("#max_buffer_count").spinner("value","10");
  function change_max_buf_count_down_limit(val){
    if(typeof val ==='object'){
      val = Math.round(val.target.value);
    }
    max_buf_count_down_limit = val;
    // console.log(max_buf_count,max_buf_count_down_limit,max_buf_count_up_limit);
  }
  function change_max_buf_count_up_limit(val){
    if(typeof val ==='object'){
      val = Math.round(val.target.value);
    }
    max_buf_count_up_limit = val;
    // console.log(max_buf_count,max_buf_count_down_limit,max_buf_count_up_limit);
  }
});
function set_buf_count_bar_val(val,max){
  $("#buf_count_bar").text(val+"/"+max);
  $("#buf_count_bar").css("width",(val/max)*100+"%");
}
function set_player_status(status){
  reset_player_status_bar();
  if(status==='init'){
    $("#player_status_text").text('未启动');
  }else if(status==='prepare'){
    $("#player_status_text").text('正在启动');
    $("#player_status_bar_container").addClass("am-active");
    $("#player_status_bar").addClass("am-progress-bar-warning");
    $("#player_status_bar").css('width','50%');
  }else if(status==='running'){
    $("#player_status_text").text('正在运行');
    $("#player_status_bar_container").addClass("am-active");
    $("#player_status_bar").addClass("am-progress-bar-success");
    $("#player_status_bar").css('width','100%');
    $("#start_player_button").attr("disabled",true);
    $("#pause_player_button").attr("disabled",false);
    $("#stop_player_button").attr("disabled",false);
  }else if(status==='pause'){
    $("#player_status_text").text('已暂停');
    $("#player_status_bar_container").removeClass("am-active");
    $("#player_status_bar").addClass("am-progress-bar-warning");
    $("#player_status_bar").css('width','70%');
    $("#start_player_button").attr("disabled",false);
    $("#pause_player_button").attr("disabled",true);
    $("#stop_player_button").attr("disabled",false);
  }else if(status==='stop'){
    $("#player_status_text").text('已停止');
    $("#player_status_bar_container").removeClass("am-active");
    $("#player_status_bar").addClass("am-progress-bar-danger");
    $("#player_status_bar").css('width','30%');
    $("#start_player_button").attr("disabled",false);
    $("#pause_player_button").attr("disabled",true);
    $("#stop_player_button").attr("disabled",true);
  }
}
function reset_player_status_bar(){
  $("#player_status_bar").css('width','0%');
  $("#player_status_bar_container").removeClass("am-active");
  $("#player_status_bar").removeClass("am-progress-bar-secondary");
  $("#player_status_bar").removeClass("am-progress-bar-success");
  $("#player_status_bar").removeClass("am-progress-bar-warning");
  $("#player_status_bar").removeClass("am-progress-bar-danger");
}