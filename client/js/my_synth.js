function user_synth_on_slide(){
  var val = $( this ).slider( "value" );
  $("#"+this.id+"_val").text(val/10+"%");
  if(node){
    node.set_property("user_synth",this.id,val/1000);
  }
}
$(document).ready(function(){
  $( "#synth_components > div" ).each(function() {
     var value = parseInt( $( this ).text(), 10 );
    $("#"+this.id+"_val").text(value/10+"%");
    $( this ).empty().slider({
      min:0,
      max:1000,
      value:value,
      slide:user_synth_on_slide,
      change:user_synth_on_slide
      });
  });
  $("#mic_gain_slider").empty().slider({
    min:-2000,
    max:2000,
    value:0,
    slide:change_mic_gain,
    change:change_mic_gain
  });
  $('#main_volume_slider').empty().slider({
    max:2000,
    min:-10000,
    value:0,
    orientation: "vertical",
    slide:change_main_gain,
    change:change_main_gain
  });
  set_mic_vol_bar(0);
  set_main_vol_bar(0);
});
$(document).ready(function(){
  for(var i=0;i<=1;i++){
    init_eq(i);
    init_delay(i);
    $("#eq_band_count_select_"+i).change(init_eq);
    $('#eq_band_active_button_'+i).click(set_eq_active);
    $('#delay_active_button_'+i).click(set_delay_active);
  }
});
function init_eq(num){
  if(typeof num =='object'){
    num = num.target.attributes['id'].value.replace('eq_band_count_select_','');
  }
  var srate = audioContext.sampleRate;
  var count = $("#eq_band_count_select_"+num).children('option:selected').val();
  $("#eq_band_container_"+num).text('');
  var html='';
  var freq_list = [];
  for(var i=0;i<count;i++){
    var freq = srate/2*Math.pow(2,-(count-i-1)/count*8);
    freq_list[i]=Math.round(freq);
    var freq_text = Math.round(freq);
    if(i===0){
      freq_text = '≤'+freq_text;
    }else if(freq>=1000){
      freq_text=Math.round(freq_text/100)/10+'k';
    }
    html=html+'<div style="float:left"><span></span><div style="text-align: center;">'+freq_text+'</div></div>';
  }
  html=html+'<div style="float:left"><div style="float:left;margin: 1em;height:6em;">+50dB</div><div style="text-align: center;">-50dB</div></div>';
  $("#eq_band_container_"+num).html(html);
  i=0;
  $("#eq_band_container_"+num+">div>span").each(function() {
    $( this ).attr('eq_num',num);
    $( this ).attr('freq',freq_list[i]);
    $( this ).empty().slider({
        orientation: "vertical",
        min:-5000,
        max:+5000,
        value:0,
        slide:set_eq_value,
        change:set_eq_value
      });
      i++;
  });
}
function init_eq_remote(num){
  if(node){
    var srate = audioContext.sampleRate;
    var count = $("#eq_band_count_select_"+num).children('option:selected').val();
    var freq_list = [];
    for(var i=0;i<count;i++){
      var freq = srate/2*Math.pow(2,-(count-i-1)/count*8);
      freq_list[i]=Math.round(freq);
    }
    node.set_property('eq_'+num,'freq_list',freq_list);
    var freq_vol = {};
    $("#eq_band_container_"+num+">div>span").each(function() {
      var freq =$(this).attr('freq'); 
      var vol = $(this).slider("value")/10;
      freq_vol[freq]=vol;
    });
    set_eq_freq_vol(num,freq_vol);
    _send_eq_active_state(num);
  }
}
function set_eq_active(eq_num){
  if(typeof eq_num =='object'){
    eq_num = eq_num.target.attributes['id'].value.replace('eq_band_active_button_','');
  }
  var active = $('#eq_band_active_button_'+eq_num).hasClass('am-active');
  if(active){
    $('#eq_band_active_button_'+eq_num).removeClass('am-active');
  }else{
    $('#eq_band_active_button_'+eq_num).addClass('am-active');
  }
  _send_eq_active_state(eq_num);
}
function _send_eq_active_state(eq_num){
  if(typeof eq_num =='object'){
    eq_num = eq_num.target.attributes['id'].value.replace('eq_band_active_button_','');
  }
  var active = $('#eq_band_active_button_'+eq_num).hasClass('am-active');
  if(node){
    node.set_property('eq_'+eq_num,'active',active);
  }
}
function set_eq_freq_vol(eq_num,freq_vol){
  if(node){
    node.set_property('eq_'+eq_num,'freq_vol',freq_vol);
  }
}
function set_eq_value(comp){
  var val = $( this ).slider( "value" );
  val /=100;
  var eq_num = $(this).attr('eq_num');
  var freq = $(this).attr('freq');
  var freq_vol = {};
  freq_vol[freq]=val;
  set_eq_freq_vol(eq_num,freq_vol);
}
function init_delay(num){
    $( "#delay_options_"+num+" > span" ).each(function() {
    var value = parseInt( $( this ).text(), 10 );
    var maxi  = parseInt( $( this ).attr('maxi'), 10 );
    $( this ).empty().slider({
      min:0,
      max:maxi,
      value: value,
      slide:set_delay_value,
      change:set_delay_value
    });
  });
}
function init_delay_remote(num){
  _send_delay_state(num);
  $( "#delay_options_"+num+" > span" ).each(function() {
    _do_set_delay_value(this);
  });
}
function set_delay_value(){
  _do_set_delay_value(this);
}
function _do_set_delay_value(comp){
  var val = $( comp ).slider( "value" );
  var mul_disp = $(comp).attr('mul_display');
  var id = $(comp).attr('id');
  var name = "delay"+id.substr(id.lastIndexOf('_'));
  $("#"+id+'_val').text(val*mul_disp);
  if(node){
    var mul_send=$(comp).attr('mul_sending');
    var option = id.replace('delay_','');
    option = option.substr(0,option.lastIndexOf('_'));
    node.set_property(name,option,val*mul_send);
  }
}
function set_delay_active(delay_num){
  if(typeof delay_num =='object'){
    delay_num = delay_num.target.attributes['id'].value.replace('delay_active_button_','');
  }
  var active = $('#delay_active_button_'+delay_num).hasClass('am-active');
  if(active){
    $('#delay_active_button_'+delay_num).removeClass('am-active');
  }else{
    $('#delay_active_button_'+delay_num).addClass('am-active');
  }
  active = !active;
  _send_delay_state(delay_num);
}
function _send_delay_state(delay_num){
  if(typeof delay_num =='object'){
    delay_num = delay_num.target.attributes['id'].value.replace('delay_active_button_','');
  }
  var active = $('#delay_active_button_'+delay_num).hasClass('am-active');
  if(node){
    node.set_property('delay_'+delay_num,'active',active);
  }
}
function set_synth_components_value_by_sliders(){
  $( "#synth_components > div" ).each(function() {
    var val = $( this ).slider( "value" );
    node.set_property("user_synth",this.id,val/1000);
  });
}
function join_node(){
  node.removeEventListener('open',set_synth_components_value_by_sliders);
  node.addEventListener('open',set_synth_components_value_by_sliders);
}
function change_mic_gain(){
  var val = $( this ).slider( "value" );
  val = val/100.0;
  mic_gain = val;
  $("#mic_gain_text").text(''+val+'dB');
}
var mic_gain = 0,main_gain=0;
var local_audio_input_enabled = false;
var min_vol_to_send = 0.003;//~-80dB
function toggle_local_audio_input(){
  local_audio_input_enabled = !local_audio_input_enabled;
  var btn = document.getElementById('local_audio_input_button');
  if(btn){
    if(local_audio_input_enabled){
      btn.classList.add('am-active');
    }else{
      btn.classList.remove('am-active');
    }
  }
}
function change_main_gain(){
  var val = $( this ).slider( "value" );
  val = val/100.0;
  main_gain = val;
  $("#gain_text").text(val+"dB");
}
function process_gain(buf){
  if(buf===undefined){
    return undefined;
  }
  var factor = Math.pow(10,main_gain/20);
  for(var i=0;i<buf.length;i++){
    var a = buf[i];
    a*=factor;
    if(a>1){
      a=1;
    }else if(a<-1){
      a=-1;
    }
    buf[i]=a;
  }
  return buf;
}
function set_mic_vol_bar(mic_vol/* in linear*/){
  var vol_bar_lo = document.getElementById('mic_vol_bar_lo');
  var vol_bar_mi = document.getElementById('mic_vol_bar_mi');
  var vol_bar_hi = document.getElementById('mic_vol_bar_hi');
  if(vol_bar_lo===undefined||vol_bar_mi===undefined||vol_bar_hi===undefined){
    return;
  }
  var vol_db = 20.0 / Math.log(10.0) * Math.log(mic_vol);
  if(vol_db>0){
    vol_db=0;
  }
  vol_bar_lo.innerText=''+Math.round(vol_db)+'dB';
  var bar_len = vol_db*2+100;
  if(bar_len<0){
    bar_len = 0;
  }else if(bar_len>100){
    bar_len = 100;
  }
  var div1=60,div2=85;
  if(bar_len<div1){
    vol_bar_lo.style.width=''+bar_len+'%';
    vol_bar_mi.style.width='0%';
    vol_bar_hi.style.width='0%';
  }else if(bar_len<div2){
    vol_bar_lo.style.width=''+div1+'%';
    vol_bar_mi.style.width=''+(bar_len-div1)+'%';
    vol_bar_hi.style.width='0%';
  }else{
    vol_bar_lo.style.width=''+div1+'%';
    vol_bar_mi.style.width=''+(div2-div1)+'%';
    vol_bar_hi.style.width=''+(bar_len-div2)+'%';
  }
}
function set_main_vol_bar(main_vol/* in linear*/){
  var vol_bar_lo = document.getElementById('main_vol_lo');
  var vol_bar_mi = document.getElementById('main_vol_mi');
  var vol_bar_hi = document.getElementById('main_vol_hi');
  var vol_text = document.getElementById('main_vol_text');
  if(vol_bar_lo===undefined||vol_bar_mi===undefined||vol_bar_hi===undefined||vol_text===undefined){
    return;
  }
  var vol_db = 20.0 / Math.log(10.0) * Math.log(main_vol);
  if(main_vol===0){
    vol_text.innerText='-∞'+'dB';
  }else{
    vol_text.innerText=''+Math.round(vol_db)+'dB';
  }
  var bar_len = vol_db*2+100;
  if(bar_len<0){
    bar_len = 0;
  }else if(bar_len>100){
    bar_len = 100;
  }
  var div1=60,div2=85;
  if(bar_len<div1){
    vol_bar_lo.style.height=''+bar_len+'%';
    vol_bar_mi.style.height='0%';
    vol_bar_hi.style.height='0%';
  }else if(bar_len<div2){
    vol_bar_lo.style.height=''+div1+'%';
    vol_bar_mi.style.height=''+(bar_len-div1)+'%';
    vol_bar_hi.style.height='0%';
  }else{
    vol_bar_lo.style.height=''+div1+'%';
    vol_bar_mi.style.height=''+(div2-div1)+'%';
    vol_bar_hi.style.height=''+(bar_len-div2)+'%';
  }
}
